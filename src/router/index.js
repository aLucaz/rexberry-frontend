import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/About.vue')
  },
  {
    path: '/gameList',
    name: 'gameList',
    component: () => import('../views/GameList.vue')
  }
];

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next)=>{
  const p = to.path.split('/');
  let path = '';

  if(p.length > 1) path = '/' + p[1];
  else path = to.path;

  const publicPages = ['/'];
  if (! publicPages.includes(path)) {
    return next('/');
  }

  next();
});

export default router
